# 什么是微服务 - 解决什么问题

Martin Fowler在2014年3月提出了Micro Service。 

	Martin Fowler是国际著名的OO专家，敏捷开发方法的创始人之一，现为ThoughtWorks公司的首席科学家.
	在面向对象分析设计、UML、模式、软件开发方法学、XP、重构等方面， 都是世界顶级的专家，
	现为Thought Works公司的首席科学家。Thought Works是一家从事企业应用开发和集成的公司。
	早在20世纪80年	代，Fowler就是使用对象技术构建多层企业应用的倡导者，
	他著有几本经典书籍:《企业应用架构模式》、《UML精粹》和《重构》等。—— 百度百科
	

## 可伸缩架构发展

### Monolithic架构
先来看看传统的web开发方式，通过对比比较容易理解什么是Microservice Architecture。和Microservice相对应的，这种方式一般被称为Monolithic(铁板一块)。所有的功能打包在一个WAR包里，基本没有外部依赖(除了容器)， 部署在一个JEE容器(Tomcat，JBoss，WebLogic)里，包含了DO/DAO，Service，UI等所有逻辑。

![](./ms-1.png)

Monolithic比较适合小项目，优点是:

* 开发简单直接，集中式管理
* 功能都在本地，没有分布式的管理开销和调用开销

它的缺点也非常明显，特别对于互联网公司来说:

* 开发效率低:所有的开发在一个项目改代码，递交代码相互等待，代码冲突不断 
* 代码维护难:代码功能耦合在一起，新人不知道何从下手 
* 部署不灵活:构建时间长，任何小修改必须重新构建整个项目，这个过程往往很长 
* 稳定性不高:一个微不足道的小问题，可以导致整个应用挂掉 
* 扩展性不够:无法满足高并发情况下的业务需求
* 重复性严重:系统之间缺乏重用，重复造轮子严重


### 微服务架构

现在主流的设计一般会采用Microservice Architecture，就是基于微服务的架构。
简单来说， __微服务的目的是有效的拆分应用，实现敏捷开发和部署 。__

![](./ms-2.png)

### Scale Cube

用《The art of scalability》一书里提到的scale cube比较容易理解如何拆分。scale cube，非常好的抽象能力，把复杂的东西用最简单的概念解释和总结。X轴代表运行多个负载均衡器之后运行的实例，Y轴代表将应用进一步分解为微服务(分库)，数据量大时，还可以用Z轴将服务按数据分区(分表)。 我们很熟悉的分库分表就是Y和Z轴代表的东西。 

![](./ms-3.png)

当互联网应用需要满足高并发，高吞吐时，我们的架构就必须考虑可伸缩性，和真正的可伸缩性只能通过分布式Scale out（横向扩展）来满足。 微服务也是为了体现Scale Cube的一种架构方式。 

## 微服务定义

官方定义：

	￼The microservice architectural style is an approach to developing a single
	application as a suite of small services， each running in its own process and 
	communicating with lightweight mechanisms， often an HTTP resource API. 
	These services are __built around business capabilities__ and independently 
	deployable by fully automated deployment machinery. There is a bare minimum of 
	centralized management of these services ， which may be written in different
	programming languages and use different data storage technologies.
	-- James Lewis and Martin Fowler
	
Martin定义大概的翻译一下就是下面几条: 

1. 一些列的独立的服务共同组成系统
2. 单独部署，跑在自己的进程里
3. 每个服务为独立的业务开发 
4. 分布式的管理

在微服务定义下，形成了一些列的标准：

* 分布式服务组成的系统
* 按照业务而不是技术来划分组织
* 做有生命的产品而不是项目
* Smart endpoints and dumb pipes(我的理解是强服务个体和弱通信) 
* 自动化运维(DevOps)
* 容错
* 快速演化

## SOA vs Microservice

除了Smart endpoints and dumb pipes都很容易理解对吗?相信很多人都会问一个问题，这是不是就是SOA换了个概念，挂羊头卖狗肉啊，有说法把Microservice叫成Lightway SOA。也有很多传统砖家跳出来说Microservice就是SOA。其实Martin 也没否认SOA和Microservice的关系。Microservice是SOA的传承，但一个最本质的区别就在于Smart endpoints and dumb pipes，或者说是真正的 分布式的、去中心化的。Smart endpoints and dumb pipes本质就是去ESB，把所有的“思考”逻辑包括路由、消息解析等 放在服务内部(Smart endpoints)，去掉一个大一统的ESB，服务间轻(dumb pipes)通信，是比SOA更彻底的拆分。






